import React, { useState, useContext, useEffect } from 'react'
import { useParams, useHistory } from 'react-router-dom'

// material-ui/core
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Paper, Typography, Button, TextField } from '@material-ui/core'

// context
import UserContext from 'components/Context/UserContext'

// components
import CustomLink from 'components/Common/CustomLink'

const useStyle = makeStyles(theme => ({
  container: {
    padding: theme.spacing(1),
  },
  btn: {
    marginRight: theme.spacing(1),
    textTransform: 'capitalize',
  },
  fileInput: {
    display: 'none',
  },
  featuredImage: {
    maxWidth: '100%',
    maxHeight: '200px',
  },
}))

function UserEdit() {
  const classes = useStyle()
  const { id } = useParams()
  const history = useHistory()

  const { userList, setUserList } = useContext(UserContext)

  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [email, setEmail] = useState('')

  useEffect(() => {
    const findUser = userList.find(user => user.id === parseInt(id, 10))
    if (findUser) {
      setFirstname(findUser.first_name)
      setLastname(findUser.last_name)
      setEmail(findUser.email)
    }
  }, [userList])

  const handleSubmit = () => {
    if (id) {
      setUserList(prev =>
        prev.map(user =>
          user.id === parseInt(id, 10)
            ? { ...user, first_name: firstname, last_name: lastname, email }
            : user,
        ),
      )
    } else {
      setUserList(prev => [
        ...prev,
        {
          id: Math.floor(Math.random() * 1000) + 10,
          first_name: firstname,
          last_name: lastname,
          email,
        },
      ])
    }
    history.push(`/user`)
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <Paper className={classes.container}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="h5">
                {id ? 'Edit User' : 'New User'}
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                label="Firstname"
                id="firstname"
                name="firstname"
                value={firstname}
                variant="outlined"
                margin="normal"
                fullWidth
                onChange={event => {
                  setFirstname(event.target.value)
                }}
                required
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <TextField
                label="Lastname"
                id="lastname"
                name="lastname"
                value={lastname}
                variant="outlined"
                margin="normal"
                fullWidth
                onChange={event => {
                  setLastname(event.target.value)
                }}
                required
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                label="Email"
                id="email"
                name="email"
                value={email}
                variant="outlined"
                margin="normal"
                fullWidth
                onChange={event => {
                  setEmail(event.target.value)
                }}
                required
              />
            </Grid>

            <Grid item xs={11}>
              <Button
                type="submit"
                variant="contained"
                color="secondary"
                className={classes.btn}
              >
                Submit
              </Button>
              <CustomLink to="/user">
                <Button
                  variant="contained"
                  color="default"
                  className={classes.btn}
                >
                  Cancel
                </Button>
              </CustomLink>
            </Grid>
          </Grid>
        </Paper>
      </form>
    </>
  )
}
export default UserEdit
