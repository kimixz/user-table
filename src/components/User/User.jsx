import React, { useContext, useState } from 'react'

// material-ui/core
import { makeStyles } from '@material-ui/core/styles'
import {
  Grid,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Button,
  IconButton,
} from '@material-ui/core'

// material-ui/icons
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'

// context
import UserContext from 'components/Context/UserContext'

// components
import CustomLink from 'components/Common/CustomLink'
import DeleteDialog from 'components/Common/DeleteDialog'

const useStyle = makeStyles(() => ({
  table: {
    minWidth: 650,
  },
  indexColumn: {
    width: '70px',
  },
  actionsColumn: {
    width: '80px',
  },
  image: {
    maxHeight: '50px',
  },
}))

function User() {
  const classes = useStyle()
  const { userList, setUserList } = useContext(UserContext)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)
  const [selectedIdDelete, setSelectedIdDelete] = useState('')

  const handleDelete = () => {
    setUserList(prev =>
      prev.filter(user => user.id !== parseInt(selectedIdDelete, 10)),
    )
  }

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <CustomLink to="/user/edit">
            <Button variant="contained" color="secondary">
              New User
            </Button>
          </CustomLink>
        </Grid>

        <Grid item xs={12}>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell className={classes.indexColumn}>Index</TableCell>
                  <TableCell>Image</TableCell>
                  <TableCell>Firstname</TableCell>
                  <TableCell>Lastname</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>Operation</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {userList.map((user, index) => (
                  <TableRow key={user.id}>
                    <TableCell>{index + 1}</TableCell>
                    <TableCell>
                      <img
                        src={user.avatar}
                        alt="user"
                        className={classes.image}
                      />
                    </TableCell>
                    <TableCell>{user.first_name}</TableCell>
                    <TableCell>{user.last_name}</TableCell>
                    <TableCell>{user.email}</TableCell>
                    <TableCell>
                      <CustomLink to={`/user/edit/${user.id}`}>
                        <IconButton>
                          <EditIcon />
                        </IconButton>
                      </CustomLink>
                      <IconButton
                        onClick={() => {
                          setOpenDeleteDialog(true)
                          setSelectedIdDelete(user.id)
                        }}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
      <DeleteDialog
        title="User"
        open={openDeleteDialog}
        handleClose={() => {
          setOpenDeleteDialog(false)
        }}
        handleDelete={() => {
          setOpenDeleteDialog(false)
          handleDelete()
        }}
      />
    </>
  )
}
export default User
