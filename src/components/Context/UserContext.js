import React from 'react';

const UserContext = React.createContext({
  userList: [],
  setUserList: () => { }
});

export default UserContext;