import React, { useEffect, useState } from 'react'
import axios from 'axios'


import {
  ThemeProvider,
  StylesProvider,
} from '@material-ui/core/styles'
import theme from 'assets/theme'

// react router
import { Switch, Route } from 'react-router-dom'

// context
import UserContext from 'components/Context/UserContext'

// components
import User from 'components/User/User'
import UserEdit from 'components/User/UserEdit'


function App() {
  const [userList, setUserList] = useState([])
  const value = { userList, setUserList };


  useEffect(() => {
    axios
      .get('https://reqres.in/api/users?page=1')
      .then(response => {
        const { data } = response.data
        setUserList(data)
      })
      .catch(error => {
        console.log(error)
      })
  }, [])

  return (
    <StylesProvider >
      <ThemeProvider theme={theme}>
        <UserContext.Provider value={value}>
          <Switch>
            <Route path="/user/edit/:id?">
              <UserEdit />
            </Route>
            <Route path="/">
              <User />
            </Route>

          </Switch>
        </UserContext.Provider>
      </ThemeProvider>
    </StylesProvider>
  )
}

export default App
